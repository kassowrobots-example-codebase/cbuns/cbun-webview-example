#!/bin/bash

mkdir assemble
cd assemble
mkdir WebviewExampleCBun
cp ../bundle.xml ./WebviewExampleCBun
cp ../app/build/outputs/apk/debug/app-debug.apk ./WebviewExampleCBun/webview_example.apk
tar --exclude='*.DS_Store' -cvzf  webview_example.cbun -C ./WebviewExampleCBun .
cp webview_example.cbun ../
cd ..
rm -rf assemble
 

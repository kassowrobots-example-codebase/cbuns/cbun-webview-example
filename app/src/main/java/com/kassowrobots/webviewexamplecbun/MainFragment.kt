package com.kassowrobots.webviewexamplecbun

import android.graphics.Bitmap
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.RenderProcessGoneDetail
import android.webkit.WebChromeClient
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.content.res.ResourcesCompat
import com.kassowrobots.api.app.KRContext
import com.kassowrobots.api.app.fragment.KRFragment
import com.kassowrobots.api.net.ConnectivityManager
import com.kassowrobots.api.net.ConnectivityManager.OnWifiSettingsResultListener.RESULT_CODE_CONNECT
import com.kassowrobots.api.util.KRLog

class MainFragment : KRFragment() {

    companion object {
        private const val URL = "https://www.kassowrobots.com/products/7-axis-collaborative-robot-arm-kr-series"
    }

    private var cm: ConnectivityManager? = null

    /**
     * Called to let the fragment to instantiate its view, ie. this method is responsible for
     * generating the fragment view.
     *
     * @param inflater Allows view inflating - not used.
     * @param container Parent view container - not used.
     * @param savedInstanceState Persistent state instance - not used.
     */
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        // instantiate the WebView as the root view of this fragment
        val webView = WebView(requireContext())

        // set the WebView background color (visible while the web content is being loaded)
        webView.background = ResourcesCompat.getDrawable(
                resources,
                R.color.panel_color_2,
                requireContext().theme
        )
        // set the webViewClient to override handling of the errors (useful for debugging)
        webView.webViewClient = object : WebViewClient() {
            override fun onReceivedError(
                view: WebView,
                req: WebResourceRequest,
                rerr: WebResourceError
            ) {
                KRLog.e(
                    "MainFragment",
                    "Error while loading " + req.url + ": " + rerr.description
                )
            }

            override fun onRenderProcessGone(
                view: WebView,
                detail: RenderProcessGoneDetail
            ): Boolean {
                KRLog.e("MainFragment", "onRenderProcessGone: " + detail.didCrash())
                return super.onRenderProcessGone(view, detail)
            }
        }
        // set the webChromeClient to avoid issues with video posters loading
        webView.webChromeClient = object : WebChromeClient() {
            override fun getDefaultVideoPoster(): Bitmap? {
                return Bitmap.createBitmap(50, 50, Bitmap.Config.ARGB_8888)
            }
        }

        cm = krContext!!.getKRService(KRContext.CONNECTIVITY_MANAGER_SERVICE) as ConnectivityManager?
        cm?.requestWifi()
        cm?.openWifiSettings { result ->
            if (result == RESULT_CODE_CONNECT) {
                // load the web content at the specified URL
                webView.loadUrl(URL)
            }
        }

        // When running the app in emulator, the connectivity manager is not available. Therefore
        // the web content is loaded directly.
        if (cm == null) {
            webView.loadUrl(URL)
        }

        return webView
    }

    override fun onDestroyView() {
        super.onDestroyView()
        cm?.releaseWifi()
    }
}